# À lire avant d'attaquer la pratique

Bienvenu sur le TP lié à l'article 3 de ma série Sécurité et Conteneurs !

## Prérequis 

D'un point de vue compétences, aucun prérequis particulier n'est attendu, mais vous devrez installer quelques outils pour pouvoir suivre les différentes étapes de l'article.

### Installer Docker

La première étape est d'installer docker sur votre ordinateur si ce n'est pas encore fait. Pour faire ces installations, tout dépend de votre OS, je vous conseille donc de suivre les conseils du [site officiel](https://docs.docker.com/get-docker/)

### Installation de Trivy

Trivy est un outil Open Source développé par AquaSecurity qui permet de faire de l'analyse d'artefact au sein des images docker. Ce moteur d'analyse permet d'avoir des rapports de vulnérabilités CVE concernant ces éléments analysés.

Pour l'installation de l'outil, je vous conseille de suivre les étapes disponibles au sein du [git du projet](https://github.com/aquasecurity/trivy)

## Construction des images docker 

Pour construire les images docker, nous utiliserons la commande suivante :
```bash
docker build -t securiteandconteneurs/art3:< TAG > < FOLDER >/.
```
Dans cette ligne il suffit de remplacer "< TAG >" par le tag à mettre sur l'image et "< FOLDER >" par le nom du dossier contenant le Dockerfile.

**ATTENTION** : Dans certains types d'installation, notamment sur les distributions Linux, il vous faudra soit précéder vos appels à la commande *docker* par un *sudo* soit ajouter le groupe docker à l'utilisateur courant.

## Exécution d'un scan Trivy

Pour scanner une image docker avec Trivy, rien de plus simple, il suffit d'utiliser la commande :
```bash
trivy < IMAGE >:< TAG >
```

